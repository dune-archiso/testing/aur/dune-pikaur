[Running every friday at 4:00 AM UTC-5](https://gitlab.com/dune-archiso/testing/dune-pikaur/-/pipeline_schedules).

<!-- ### Dune modules

| FreeBSD ports | Arch User Repository                                                                                                                                                            | openSUSE Tumbleweed experimental | Debian experimental |
| :------------ | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | :------------------------------- | :------------------ |
| x             | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-common.svg?header=dune-common)](https://aur.archlinux.org/packages/dune-common)                            | x                                | x                   |
|               | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-geometry.svg?header=dune-geometry)](https://aur.archlinux.org/packages/dune-geometry)                      |                                  |
|               | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-istl.svg?header=dune-istl)](https://aur.archlinux.org/packages/dune-istl)                                  |                                  |
|               | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-localfunctions.svg?header=dune-localfunctions)](https://aur.archlinux.org/packages/dune-localfunctions)    |                                  |
|               | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-alugrid.svg?header=dune-alugrid)](https://aur.archlinux.org/packages/dune-alugrid)                         |                                  |
|               | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-grid.svg?header=dune-grid)](https://aur.archlinux.org/packages/dune-grid)                                  |                                  |
|               | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-functions.svg?header=dune-functions)](https://aur.archlinux.org/packages/dune-functions)                   |                                  |
|               | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-logging.svg?header=dune-logging)](https://aur.archlinux.org/packages/dune-logging)                         |                                  |
|               | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-multidomaingrid.svg?header=dune-multidomaingrid)](https://aur.archlinux.org/packages/dune-multidomaingrid) |                                  |
|               | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-typetree.svg?header=dune-typetree)](https://aur.archlinux.org/packages/dune-typetree)                      |                                  |
|               | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-uggrid.svg?header=dune-uggrid)](https://aur.archlinux.org/packages/dune-uggrid)                            |                                  |
|               | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-spgrid.svg?header=dune-spgrid)](https://aur.archlinux.org/packages/dune-spgrid)                            |                                  |
|               | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-foamgrid.svg?header=dune-foamgrid)](https://aur.archlinux.org/packages/dune-foamgrid)                      |                                  |
|               | [![AUR package](https://repology.org/badge/version-for-repo/aur/dune-pdelab.svg?header=dune-pdelab)](https://aur.archlinux.org/packages/dune-pdelab)                            |                                  |

[![FreeBSD port](https://repology.org/badge/version-for-repo/freebsd/dune-common.svg?header=FreeBSD%20port%20dune-common)]()

https://build.opensuse.org/repositories/science/dune-common

https://packages.debian.org/source/experimental/dune-common

https://repology.org/project/awesome/badges

[conflicts with dune-alugrid: both install share/dune/cmake/modules/FindPThreads.cmake](https://gitlab.dune-project.org/dune-fem/dune-fem/-/issues/111)

https://gitlab.dune-project.org/core/dune-grid/-/issues/137

https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/548 -->
